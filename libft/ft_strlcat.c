/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strlcat.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:35:08 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/12 16:52:25 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t maxlen)
{
	size_t	len;
	size_t	srclen;
	size_t	dstlen;

	if (!dst && !src)
		return (0);
	srclen = ft_strlen(src);
	dstlen = 0;
	while (dst[dstlen] != '\0' && dstlen < maxlen)
		dstlen++;
	len = srclen + dstlen;
	if (maxlen > 0 && maxlen > ft_strlen(dst))
	{
		if (srclen < (maxlen - dstlen))
			ft_memcpy(dst + dstlen, src, srclen + 1);
		else
		{
			ft_memcpy(dst + dstlen, src, maxlen - dstlen - 1);
			dst[maxlen - 1] = '\0';
		}
	}
	return (len);
}
