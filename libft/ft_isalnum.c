/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_isalnum.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:38:32 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/14 15:24:42 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_isalnum(int c)
{
	if (ft_isdigit(c) || ft_isalpha(c))
		return (1);
	return (0);
}
