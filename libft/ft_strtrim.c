/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strtrim.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:36:07 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:46:29 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	is_set(char *str, char const *set, int index)
{
	unsigned long	j;

	j = 0;
	while (j < ft_strlen(set))
	{
		if (str[index] == set[j])
			return (1);
		j++;
	}
	return (0);
}

static size_t	ch_c(char *s, char const *set)
{
	size_t	i;
	size_t	counter;

	counter = 0;
	i = ft_strlen(s) - 1;
	while (is_set(s, set, counter) && s[counter] != '\0')
	{
		if (s[counter + 1] == '\0')
			return (counter);
		counter++;
	}
	while (is_set(s, set, i) && s[i] > 0)
	{
		counter++;
		i--;
	}
	return (counter);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*ns1;
	int		i;
	size_t	n;

	i = 0;
	if (!s1)
		return (0);
	if (ft_strlen(s1) == 0 || ch_c((char *)s1, set) == ft_strlen(s1))
	{
		ns1 = ft_calloc(sizeof(char *), 1);
		if (!ns1)
			return (0);
		return (ns1);
	}
	ns1 = (char *)malloc((ft_strlen(s1) - ch_c((char *)s1, set) + 1) * 1);
	if (!ns1)
		return (0);
	while (is_set((char *)s1, set, i))
		i++;
	n = ft_strlen(s1) - ch_c((char *)s1, set);
	ft_memcpy(ns1, s1 + i, n);
	ns1[n] = '\0';
	return (ns1);
}
