/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_split.c                                         :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/06 10:33:46 by eteppo        #+#    #+#                 */
/*   Updated: 2021/05/16 17:44:27 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	ft_str_counter(char const *s, char c)
{
	size_t	i;
	size_t	counter;

	i = 0;
	counter = 0;
	if (s[0] != c)
		counter++;
	while (i < ft_strlen(s))
	{
		if (s[i] == c && s[i + 1] != c && s[i + 1] != '\0')
			counter++;
		i++;
	}
	return (counter);
}

static size_t	ft_substr_max_len(char const *s, char c)
{
	size_t	i;
	size_t	maxvalue;
	size_t	maxcounter;

	i = 0;
	maxvalue = 0;
	while (s[i] == c && s[i] != '\0')
		i++;
	while (i < ft_strlen(s))
	{
		maxcounter = 0;
		while (s[i] != c && s[i] != '\0')
		{
			maxcounter++;
			i++;
		}
		if (maxcounter > maxvalue)
			maxvalue = maxcounter;
		i++;
	}
	return (maxvalue);
}

static int	check_init(char **ptr_arr, size_t max_len, size_t str_count)
{
	int	i;

	if (!ptr_arr)
		return (0);
	i = 0;
	while (i < (int)str_count)
	{
		ptr_arr[i] = ft_calloc(sizeof(size_t), max_len + 1);
		if (!ptr_arr[i])
		{
			while (i >= 0)
			{
				free(ptr_arr[i]);
				i--;
			}
			free(ptr_arr);
			return (0);
		}
		i++;
	}
	return (1);
}

static char	**ft_fill_array(char const *s, char **ptr_array, char c)
{
	size_t	i;
	size_t	j;
	size_t	k;

	i = 0;
	k = 0;
	while (k < ft_strlen(s))
	{
		j = 0;
		while (s[k] == c && s[k] != '\0')
			k++;
		while (s[k] != c && s[k] != '\0')
		{
			ptr_array[i][j] = s[k];
			j++;
			k++;
			if (s[k] == c || s[k] == '\0')
				ptr_array[i][j] = '\0';
		}
		i++;
		k++;
	}
	return (ptr_array);
}

char	**ft_split(char const *s, char c)
{
	char	**ptr_array;

	if (!s)
		return (0);
	if (ft_strlen(s) == 0)
	{
		ptr_array = ft_calloc(sizeof(char *), 1);
		if (!ptr_array)
			return (0);
		return (ptr_array);
	}
	ptr_array = ft_calloc(sizeof(char *), ft_str_counter(s, c) + 1);
	if (!check_init(ptr_array, ft_substr_max_len(s, c), ft_str_counter(s, c)))
		return (0);
	ft_fill_array(s, ptr_array, c);
	return (ptr_array);
}
