/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_substr.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/01 18:18:53 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/16 16:48:30 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char			*substr;
	unsigned int	i;

	if (!s)
		return (0);
	if (start >= ft_strlen(s))
		len = 0;
	else if (start < ft_strlen(s) && start + len > ft_strlen(s))
		len = (ft_strlen(s) - start);
	substr = ft_calloc(sizeof(char), len + 1);
	if (!substr)
		return (0);
	if (start < ft_strlen(s))
	{
		i = 0;
		while (i < len)
		{
			substr[i] = s[i + start];
			i++;
		}
	}
	return (substr);
}
