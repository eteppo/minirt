/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_strmapi.c                                       :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2020/11/11 17:21:36 by eteppo        #+#    #+#                 */
/*   Updated: 2020/11/14 14:26:15 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*newstring;
	size_t	i;

	if (!s)
		return (0);
	i = 0;
	newstring = (char *)malloc(sizeof(char) * (ft_strlen(s) + 1));
	if (!newstring)
		return (0);
	while (i < ft_strlen(s))
	{
		newstring[i] = f(i, s[i]);
		i++;
	}
	newstring[i] = '\0';
	return (newstring);
}
