/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ft_is_in_set.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/15 14:13:14 by eteppo        #+#    #+#                 */
/*   Updated: 2022/01/17 09:57:42 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_is_in_set(char c, char *set)
{
	int	i;

	i = 0;
	if (set)
	{
		while (set[i])
		{
			if (set[i] == c)
				return (1);
			i++;
		}
	}
	return (0);
}
