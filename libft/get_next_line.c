/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   get_next_line.c                                    :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/03/25 10:27:39 by eteppo        #+#    #+#                 */
/*   Updated: 2022/01/17 11:31:12 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*copy_str(char *str, int len)
{
	char	*ptr;
	int		i;

	ptr = NULL;
	i = 0;
	if (str)
	{
		ptr = malloc(sizeof(char) * len + 1);
		if (!ptr)
			return (0);
		while (str[i] && i < len)
		{
			ptr[i] = str[i];
			i++;
		}
		ptr[i] = '\0';
		free(str);
	}
	return (ptr);
}

static int	read_and_save(int fd, t_var *var)
{
	while (1)
	{
		var->ret = read(fd, &var->c, 1);
		if (var->ret < 0)
			return (-1);
		if (var->ret == 0 || var->c == '\n')
			break ;
		var->str[var->counter] = var->c;
		var->counter++;
		if (var->counter == var->len)
		{
			var->len *= 2;
			var->str = copy_str(var->str, var->len);
			if (!var->str)
				return (-1);
		}
	}
	var->str = copy_str(var->str, var->counter);
	if (!var->str)
		return (-1);
	var->str[var->counter] = '\0';
	return (0);
}

int	get_next_line(int fd, char **line)
{
	t_var	var;

	var.len = 3;
	var.counter = 0;
	var.c = '\0';
	if (!line || fd < 0)
		return (-1);
	var.str = malloc(sizeof(char) * var.len + 1);
	if (!var.str)
		return (-1);
	var.str[var.len] = '\0';
	if (read_and_save(fd, &var) == -1)
		return (-1);
	*line = var.str;
	return (var.ret);
}
