/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   minirt.h                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/14 11:30:23 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/22 12:33:45 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINIRT_H
# define MINIRT_H

# include "../mlx/mlx.h"
# include "../libft/libft.h"
# include <fcntl.h>
# include <unistd.h>
# include <stdio.h>
# include <math.h>
# include <errno.h>

# define FLT_MAX 340282346638528859811704183484516925440.000000

typedef struct s_float3
{
	float	x;
	float	y;
	float	z;
}				t_float3;

typedef struct s_int3
{
	int	x;
	int	y;
	int	z;
}				t_int3;

typedef struct s_ambient
{
	float	ratio;
	t_int3	rgb;
}				t_ambient;

typedef struct s_ray
{
	t_float3	place;
	t_float3	vector;
}				t_ray;

typedef struct s_camera
{
	t_float3	place;
	t_float3	vector;
	int			fov;
}				t_camera;

typedef struct s_light
{
	t_float3	place;
	float		ratio;
	t_int3		rgb;
}				t_light;

typedef struct s_sphere
{
	t_float3		center;
	float			diameter;
	t_int3			rgb;
}				t_sphere;

typedef struct s_plane
{
	t_float3		center;
	t_float3		vector;
	t_int3			rgb;
}				t_plane;

typedef struct s_cylinder
{
	t_float3			center;
	t_float3			vector;
	float				diameter;
	float				height;
	t_int3				rgb;
}				t_cylinder;

typedef struct s_abc_form
{
	float			a;
	float			b;
	float			c;
	float			root1;
	float			root2;
}				t_abc_form;

typedef union u_object
{
	t_sphere	sphere;
	t_plane		plane;
	t_cylinder	cylinder;
}				t_object;

typedef struct matrices
{
	float		final[16];
	float		inverse[16];
}				t_matrices;

typedef struct s_obj_list
{
	char				object_type;
	t_object			object;
	struct s_obj_list	*next;
}				t_obj_list;

typedef struct s_hitpoint
{
	t_float3		place;
	t_obj_list		*node;
	float			distance_to_camera;
	char			cylinder_hitpart;
	t_float3		hitpoint_projection;
	int				inside_cylinder;
	t_float3		normal;
	int				is_object_lit;
}				t_hitpoint;

typedef struct s_data
{
	int			width;
	int			height;
	t_obj_list	*object_list;
	t_ambient	ambient;
	t_camera	camera;
	float		cam_to_world_matrix[16];
	t_light		light;
	int			ambient_is_set;
	int			camera_is_set;
	int			light_is_set;
}				t_data;
typedef struct s_screen
{
	void	*mlx;
	void	*window;
	void	*image_ptr;
	char	*address;
	int		bits_per_pixel;
	int		line_length;
	int		endian;
}				t_screen;

typedef struct s_scene
{
	t_screen	screen;
	t_data		data;
}				t_scene;

//		--- Color ---
// color.c
t_int3			calculate_sphere_color(t_scene *scene, t_ray lightray,
					t_hitpoint hitpoint, t_int3 ambient_rgb);
t_int3			calculate_plane_color(t_scene *scene, t_ray lightray,
					t_hitpoint hitpoint, t_int3 ambient_rgb);
t_int3			calculate_cylinder_color(t_scene *scene, t_ray lightray,
					t_hitpoint hitpoint, t_int3 ambient_rgb);
unsigned int	calculate_color(t_scene *scene, t_hitpoint hitpoint);

// rgb.c
unsigned int	rgb_to_uint(t_int3 vec);
t_int3			int3_value_addition(t_int3 rgb, t_int3 ambient);
t_int3			int3_value_multiplication(t_int3 rgb, float value);
t_float3		int3_division(t_int3 ambient);
t_int3			ambient_multiplied_by_color(t_int3 object_color,
					t_int3 ambient);

// color_utils.c
int				max(int color);
int				places_are_equal(t_float3 place1, t_float3 place2);

//		--- Error ---
// error.c
void			perror_and_exit(int errno_value);
void			write_error_and_exit(char *str);
void			free_array(char **array);

//		--- Init ---
// init.c
void			init_float3(t_float3 *location);
t_int3			init_rgb(void);
void			init_screen(t_screen *screen, int width, int height);
void			init_data(t_data *data, int width, int height);
void			init_scene(t_scene *scene, int width, int height);

//		--- Lists ---
// list.c
int				ft_obj_lstsize(t_obj_list *lst);
t_obj_list		*ft_obj_lstnew(t_object *object, char object_type);
void			ft_obj_lstadd_back(t_obj_list **lst, t_obj_list *new);
int				save_to_object_list(t_data *data, t_object *object,
					char obj_type);

//		--- Main ---
// main.c
int				main(int argc, char **argv);

//		--- Matrix ---
// get_matrices.c
void			get_cylinder_matrices(t_cylinder cylinder, t_matrices *matrix);
void			get_camera_matrix(t_scene *scene);

// invert_matrix.c
void			invert_continued_1(float *m, float *inv);
void			invert_continued_2(float *m, float *inv);
void			create_invert_matrix(float *m, float *inverse);

// matrix.c
t_float3		compute_forward(float *matrix, t_ray ray);
float			absolute_value(float value);
t_float3		compute_right(float *matrix, t_float3 forward, int is_cylinder);
void			compute_up(float *matrix, t_float3 forward, t_float3 right);
void			create_matrix(float *matrix, t_ray ray, int is_cylinder);

// multiply_matrix.c
t_float3		float3_matrix_multiplied(float *matrix, t_float3 old_vector,
					int fourth_value);
void			matrix_multiplication(float *matrix1, float *matrix2,
					float *result);

//		--- Parsing ---
// parse_ACL.c
int				is_acl_complete(t_data data);
void			save_ambient(t_data *data, char *line);
void			save_camera(t_data *data, char *line);
void			save_light(t_data *data, char *line);

// parse_input.c ---
void			parse_and_save_line(t_data *data, char *str);
void			check_file_extension(char *file);
void			parse_input(t_data *data, char *file);

// parse_objects.c
void			save_plane(t_data *data, char *line);
void			save_sphere(t_data *data, char *line);
void			save_cylinder(t_data *data, char *line);

// parse_utils.c
int				save_vector(t_float3 *direction, char *line);
int				save_rgb(t_int3 *rgb, char *str);
int				save_coordinates(t_float3 *place, char *str);
int				save_ratio(float *ratio, char *ratio_str);
int				save_float(float *flt, char *str);

// parse_utils2.c
int				count_split(char **split);
int				is_in_range(float number, float lower, float upper);
int				is_in_set(char c, char *set);
int				is_line_invalid(char *str, char *allowed_chars);
int				check_fov(int *fov, char *line);

//			--- Ray ---
// Cylinder.c
void			find_local_intersection_middle(t_hitpoint *hitpoint_middle,
					t_cylinder cylinder, t_ray ray);
t_hitpoint		get_hitpoint_middle(t_ray local_ray, t_hitpoint hitpoint,
					t_matrices matrix, char hitpart);
void			find_local_intersection_disc(t_hitpoint *temp_hitpoint,
					t_ray local_disc,
					t_ray local_ray, float diameter);
t_hitpoint		get_hitpoint_disc(t_ray local_ray, t_hitpoint hitpoint,
					t_matrices matrix, char hitpart);

// intersect.c
t_hitpoint		intersect_sphere(t_ray ray, t_obj_list *node);
t_hitpoint		intersect_plane(t_ray ray, t_obj_list *node);
t_hitpoint		intersect_cylinder(t_ray ray, t_obj_list *node);
t_hitpoint		intersect_object_list(t_ray ray, t_obj_list *object);
t_hitpoint		light_intersect_object_list(t_ray ray, t_obj_list *node,
					float max_distance);

// ray.c
void			create_lightray(t_scene scene, t_hitpoint hitpoint,
					t_ray *lightray);
int				is_object_lit(t_scene scene, t_hitpoint *hitpoint, t_ray ray,
					t_ray lightray);
t_ray			create_ray(t_scene scene, int x, int y);
void			shoot_ray(t_scene scene, int x, int y);
void			build_camera_rays(t_scene scene);

// utils.c
void			my_mlx_pixel_put(t_scene scene, int x, int y,
					unsigned int color);
void			quadratic_extended(t_abc_form *abc, float q,
					float discriminant);
int				solve_quadratic(t_abc_form *abc);
int				is_in_sphere(t_scene scene, t_hitpoint hitpoint);
int				is_in_cylinder(t_float3 ray_place,
					t_hitpoint hitpoint);

//			--- Vector math ---
// vector_math.c
float			dot_product(t_float3 a, t_float3 b);
t_float3		cross_product(t_float3 vector_a, t_float3 vector_b);
void			normalize_vector(t_float3 *line);
float			calculate_vector_length(t_float3 vector);
t_float3		get_cylinder_normal(t_hitpoint *hitpoint, t_ray ray);

// vector_math2.c
t_float3		float3_addition(t_float3 a, t_float3 b);
t_float3		float3_subtraction(t_float3 value, t_float3 subtractor);
t_float3		float3_value_multiplication(t_float3 vec, float value);
void			calculate_intersection_coordinates(t_hitpoint *hitpoint,
					t_ray ray);

#endif