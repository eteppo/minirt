# **************************************************************************** #
#                                                                              #
#                                                         ::::::::             #
#    Makefile                                           :+:    :+:             #
#                                                      +:+                     #
#    By: akant <akant@student.codam.nl>               +#+                      #
#                                                    +#+                       #
#    Created: 2022/01/12 15:54:55 by akant         #+#    #+#                  #
#    Updated: 2022/03/22 12:29:48 by eteppo        ########   odam.nl          #
#                                                                              #
# **************************************************************************** #

NAME = miniRT

SOURCES = main.c error.c init.c list.c parse_ACL.c \
			parse_input.c parse_object.c parse_utils.c parse_utils2.c \
			intersect.c ray.c color.c rgb.c color_utils.c \
			get_matrices.c invert_matrix.c \
			multiply_matrix.c matrix.c hitpoint_cylinder.c \
			vector_math.c vector_math2.c utils.c \

VPATH = ./srcs/error ./srcs/init ./srcs/list ./srcs/main ./srcs/parsing \
			./srcs/ray ./srcs/color ./srcs/matrix ./srcs/vector_math

OBJS_DIR = ./objs

OBJECTS = $(addprefix $(OBJS_DIR)/, $(SOURCES:.c=.o))

LIBFT_LIBRARY = ./libft/libft.a

MLX_LIBRARY = ./mlx/libmlx.dylib

FLAGS = -Wall -Werror -Wextra -fsanitize=address -fsanitize=leak

CC = gcc

all: $(NAME)

$(NAME): $(OBJECTS) $(LIBFT_LIBRARY) $(MLX_LIBRARY)
	$(CC) $(OBJECTS) $(LIBFT_LIBRARY) $(MLX_LIBRARY) -Lmlx -lmlx -framework \
	OpenGL -framework AppKit -lz -o $(NAME)

$(LIBFT_LIBRARY):
	$(MAKE) -C libft
	cp $(LIBFT_LIBRARY) .

$(MLX_LIBRARY):
	$(MAKE) -C mlx
	cp $(MLX_LIBRARY) .

$(OBJS_DIR)/%.o: %.c
	$(CC) $(CFLAGS) -Imlx -c $< -o $@

clean:
	rm -f $(OBJS_DIR)/*.o
	$(MAKE) -C libft clean
	$(MAKE) -C mlx clean

fclean: clean
	rm -f $(NAME) libft.a libmlx.dylib

re: fclean all

.PHONY: all clean fclean re
