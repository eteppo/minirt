## A basic raytracer with miniLibX in C.

![](images/submarine.png)

The project is made by implementing the most basic ray tracing features.  
  
Handles three different geometric objects: plane, sphere and cylinder.

1. clone the repo:  
``git clone git@gitlab.com:eteppo/minirt.git``  

2. cd into it and make:  
``cd minirt && make``  

3. run different scenes from the scenes folder, f. ex:  
``./minirt scenes/eval/multi-objects2.rt``