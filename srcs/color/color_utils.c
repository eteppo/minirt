/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   color_utils.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/21 13:21:29 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/22 12:38:59 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include	"../../includes/minirt.h"

int	max(int color)
{
	if (color > 255)
		color = 255;
	return (color);
}

int	places_are_equal(t_float3 place1, t_float3 place2)
{
	if (absolute_value(place1.x - place2.x) < 1e-3
		&& absolute_value(place1.y - place2.y) < 1e-3
		&& absolute_value(place1.z - place2.z) < 1e-3)
		return (1);
	return (0);
}
