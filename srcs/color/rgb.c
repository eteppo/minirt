/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   rgb.c                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/23 14:44:47 by akant         #+#    #+#                 */
/*   Updated: 2022/03/22 12:29:12 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include	"../../includes/minirt.h"

unsigned int	rgb_to_uint(t_int3 vec)
{
	unsigned int	rgb;

	rgb = vec.x;
	rgb = (rgb << 8) + vec.y;
	rgb = (rgb << 8) + vec.z;
	return (rgb);
}

t_int3	int3_value_addition(t_int3 rgb, t_int3 ambient)
{
	t_int3	new_color;

	new_color.x = max(rgb.x + ambient.x);
	new_color.y = max(rgb.y + ambient.y);
	new_color.z = max(rgb.z + ambient.z);
	return (new_color);
}

t_int3	int3_value_multiplication(t_int3 rgb, float value)
{
	t_int3	temp;

	temp.x = rgb.x * value;
	temp.y = rgb.y * value;
	temp.z = rgb.z * value;
	return (temp);
}

t_float3	int3_division(t_int3 ambient)
{
	t_float3	result;

	result.x = (float)ambient.x / 255;
	result.y = (float)ambient.y / 255;
	result.z = (float)ambient.z / 255;
	return (result);
}

t_int3	ambient_multiplied_by_color(t_int3 object_color, t_int3 ambient)
{
	t_float3	ambient_strength;
	t_int3		ambient_color;

	ambient_strength = int3_division(ambient);
	ambient_color.x = ambient_strength.x * object_color.x;
	ambient_color.y = ambient_strength.y * object_color.y;
	ambient_color.z = ambient_strength.z * object_color.z;
	return (ambient_color);
}
