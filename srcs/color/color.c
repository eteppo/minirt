/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   color.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/18 14:45:11 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/27 11:40:54 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include	"../../includes/minirt.h"

t_int3	calculate_sphere_color(t_scene *scene, t_ray lightray,
	t_hitpoint hitpoint, t_int3 ambient_rgb)
{
	float		facing_ratio;
	t_hitpoint	closest_to_light;
	t_int3		rgb;
	t_int3		ambient;

	facing_ratio = dot_product(hitpoint.normal,
			float3_value_multiplication(lightray.vector, -1));
	closest_to_light = intersect_object_list(lightray, scene->data.object_list);
	rgb = init_rgb();
	ambient_rgb = ambient_multiplied_by_color(hitpoint.node->object.sphere.rgb,
			ambient_rgb);
	if (!hitpoint.is_object_lit)
		return (ambient_rgb);
	if (closest_to_light.distance_to_camera != INFINITY
		&& closest_to_light.node->object_type == 's'
		&& places_are_equal(closest_to_light.place, hitpoint.place))
		return (int3_value_addition(
				int3_value_multiplication(int3_value_multiplication(
						hitpoint.node->object.sphere.rgb,
						scene->data.light.ratio), facing_ratio), ambient_rgb));
	return (ambient_rgb);
}

t_int3	calculate_plane_color(t_scene *scene, t_ray lightray,
		t_hitpoint hitpoint, t_int3 ambient_rgb)
{
	float		facing_ratio;
	t_hitpoint	closest_to_light;
	t_int3		rgb;

	facing_ratio = dot_product(hitpoint.normal,
			float3_value_multiplication(lightray.vector, -1));
	if (facing_ratio < 0)
		facing_ratio *= -1;
	closest_to_light = intersect_object_list(lightray, scene->data.object_list);
	rgb = init_rgb();
	ambient_rgb = ambient_multiplied_by_color(hitpoint.node->object.plane.rgb,
			ambient_rgb);
	if (!hitpoint.is_object_lit)
		return (ambient_rgb);
	if (closest_to_light.distance_to_camera != INFINITY
		&& places_are_equal(closest_to_light.place, hitpoint.place)
		&& closest_to_light.node->object_type == 'p')
	{
		rgb = int3_value_multiplication(hitpoint.node->object.plane.rgb,
				scene->data.light.ratio);
		rgb = int3_value_multiplication(rgb, facing_ratio);
		rgb = int3_value_addition(rgb, ambient_rgb);
		return (rgb);
	}
	return (ambient_rgb);
}

t_int3	calculate_cylinder_color(t_scene *scene, t_ray lightray,
		t_hitpoint hitpoint, t_int3 ambient_rgb)
{
	float		facing_ratio;
	t_int3		rgb;
	t_hitpoint	closest_to_light;
	float		max_distance;

	rgb = init_rgb();
	max_distance = calculate_vector_length(float3_subtraction(hitpoint.place,
				lightray.place)) - 15e-3;
	facing_ratio = dot_product(hitpoint.normal,
			float3_value_multiplication(lightray.vector, -1));
	closest_to_light = light_intersect_object_list(lightray,
			scene->data.object_list, max_distance);
	ambient_rgb = ambient_multiplied_by_color(
			hitpoint.node->object.cylinder.rgb, ambient_rgb);
	if (!hitpoint.is_object_lit)
		return (ambient_rgb);
	if (closest_to_light.distance_to_camera > max_distance)
	{
		rgb = int3_value_multiplication(hitpoint.node->object.cylinder.rgb,
				scene->data.light.ratio);
		rgb = int3_value_multiplication(rgb, facing_ratio);
		rgb = int3_value_addition(rgb, ambient_rgb);
		return (rgb);
	}
	return (ambient_rgb);
}

unsigned int	calculate_color(t_scene *scene, t_hitpoint hitpoint)
{
	t_int3	color;
	t_ray	lightray;
	t_int3	ambient_rgb;

	create_lightray(*scene, hitpoint, &lightray);
	ambient_rgb = int3_value_multiplication(scene->data.ambient.rgb,
			scene->data.ambient.ratio);
	if (hitpoint.node->object_type == 's')
		color = calculate_sphere_color(scene, lightray, hitpoint, ambient_rgb);
	else if (hitpoint.node->object_type == 'p')
		color = calculate_plane_color(scene, lightray, hitpoint, ambient_rgb);
	else
		color = calculate_cylinder_color(scene, lightray, hitpoint,
				ambient_rgb);
	return (rgb_to_uint(color));
}
