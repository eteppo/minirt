/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse_utils.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/13 11:18:22 by akant         #+#    #+#                 */
/*   Updated: 2022/02/23 12:37:58 by akant         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

int	save_vector(t_float3 *direction, char *line)
{
	if (save_coordinates(direction, line) == -1)
		return (-1);
	if (!is_in_range(direction->x, -1, 1) || !is_in_range(direction->y, -1, 1)
		|| !is_in_range(direction->z, -1, 1))
		return (-1);
	if (sqrt(direction->x * direction->x + direction->y * direction->y
			+ direction->z * direction->z) < 0.999
		|| sqrt(direction->x * direction->x + direction->y * direction->y
			+ direction->z * direction->z) > 1.001)
		return (-1);
	return (0);
}

int	save_rgb(t_int3 *rgb, char *str)
{
	char	**split;
	int		error;

	error = 0;
	split = ft_split(str, ',');
	if (!split)
		return (-1);
	if (count_split(split) != 3)
		return (-1);
	rgb->x = ft_atoi_with_error(split[0], &error);
	if (error == -1)
		return (-1);
	rgb->y = ft_atoi_with_error(split[1], &error);
	if (error == -1)
		return (-1);
	rgb->z = ft_atoi_with_error(split[2], &error);
	if (error == -1)
		return (-1);
	if (!is_in_range(rgb->x, 0, 255) || !is_in_range(rgb->y, 0, 255)
		|| !is_in_range(rgb->z, 0, 255))
		return (-1);
	return (0);
}

int	save_coordinates(t_float3 *location, char *str)
{
	char	**split;
	int		error;

	error = 0;
	split = ft_split(str, ',');
	if (!split)
		return (-1);
	if (count_split(split) != 3)
		return (-1);
	location->x = ft_atof_with_error(split[0], &error);
	if (error == -1)
		return (-1);
	location->y = ft_atof_with_error(split[1], &error);
	if (error == -1)
		return (-1);
	location->z = ft_atof_with_error(split[2], &error);
	if (error == -1)
		return (-1);
	return (0);
}

int	save_ratio(float *ratio, char *ratio_str)
{
	float	number;
	int		error;

	error = -1;
	number = ft_atof_with_error(ratio_str, &error);
	if (error == -1)
		return (-1);
	if (number < 0 || number > 1)
		return (-1);
	*ratio = number;
	return (0);
}

int	save_float(float *flt, char *str)
{
	float	number;
	int		error;

	error = -1;
	number = ft_atof_with_error(str, &error);
	if (error == -1)
		return (-1);
	if (number <= 0)
		return (-1);
	*flt = number;
	return (0);
}
