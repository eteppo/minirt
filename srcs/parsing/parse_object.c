/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse_object.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/14 11:32:03 by akant         #+#    #+#                 */
/*   Updated: 2022/03/06 18:34:37 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	save_plane(t_data *data, char *line)
{
	char		**split;
	t_object	object;

	line += 2;
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_plane");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_plane");
	if (count_split(split) != 3)
		write_error_and_exit("Wrong word count; save_plane");
	if (save_coordinates(&(object.plane.center), split[0]) == -1)
		write_error_and_exit("Coordinates are not correct; save_plane");
	if (save_vector(&(object.plane.vector), split[1]) == -1)
		write_error_and_exit("Vector not normalized; save_plane");
	if (save_rgb(&(object.plane.rgb), split[2]) == -1)
		write_error_and_exit("Wrong rgb; save_plane");
	if (save_to_object_list(data, &object, 'p') == -1)
		write_error_and_exit("Object malloc failed; save_plane");
	free_array(split);
}

void	save_sphere(t_data *data, char *line)
{
	char		**split;
	t_object	object;

	line += 2;
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_sphere");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_sphere");
	if (count_split(split) != 3)
		write_error_and_exit("Wrong word count; save_sphere");
	if (save_coordinates(&(object.sphere.center), split[0]) == -1)
		write_error_and_exit("Coordinates are invalid; save_sphere");
	if (save_float(&(object.sphere.diameter), split[1]) == -1)
		write_error_and_exit("Diameter is invalid; save_sphere");
	if (save_rgb(&(object.sphere.rgb), split[2]) == -1)
		write_error_and_exit("Rgb failed; save_sphere");
	if (save_to_object_list(data, &object, 's') == -1)
		write_error_and_exit("Object malloc failed; save_sphere");
	free_array(split);
}

void	save_cylinder(t_data *data, char *line)
{
	char		**split;
	t_object	object;

	line += 2;
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_cylinder");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_cylinder");
	if (count_split(split) != 5)
		write_error_and_exit("Wrong word count; save_cylinder");
	if (save_coordinates(&(object.cylinder.center), split[0]) == -1)
		write_error_and_exit("Coordinates are invalid; save_cylinder");
	if (save_vector(&(object.cylinder.vector), split[1]) == -1)
		write_error_and_exit("Vector not normalized; save_cylinder");
	if (save_float(&(object.cylinder.diameter), split[2]) == -1)
		write_error_and_exit("Diameter is invalid; save_cylinder");
	if (save_float(&(object.cylinder.height), split[3]) == -1)
		write_error_and_exit("Height is invalid; save_cylinder");
	if (save_rgb(&(object.cylinder.rgb), split[4]) == -1)
		write_error_and_exit("Rgb failed; save_cylinder");
	if (save_to_object_list(data, &object, 'c') == -1)
		write_error_and_exit("Object malloc failed; save_cylinder");
	free_array(split);
}
