/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse_ACL.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/13 10:51:27 by akant         #+#    #+#                 */
/*   Updated: 2022/03/18 13:51:41 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

int	is_acl_complete(t_data data)
{
	if (data.ambient_is_set && data.camera_is_set
		&& data.light_is_set)
		return (1);
	return (0);
}

void	save_ambient(t_data *data, char *line)
{
	char	**split;

	line++;
	data->ambient_is_set += 1;
	if (data->ambient_is_set > 1)
		write_error_and_exit("More than one Ambient; save_ambient");
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_ambient");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_ambient");
	if (count_split(split) != 2)
		write_error_and_exit("Wrong word count; save_ambient");
	if (save_ratio(&(data->ambient.ratio), split[0]) == -1)
		write_error_and_exit("Wrong ratio; save_ambient");
	if (save_rgb(&(data->ambient.rgb), split[1]) == -1)
		write_error_and_exit("Wrong rgb; save_ambient");
	free_array(split);
}

void	save_camera(t_data *data, char *line)
{
	char	**split;

	line++;
	data->camera_is_set += 1;
	if (data->camera_is_set > 1)
		write_error_and_exit("More than one camera; save_camera");
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_camera");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_camera");
	if (count_split(split) != 3)
		write_error_and_exit("Wrong word count; save_camera");
	if (save_coordinates(&(data->camera.place), split[0]) == -1)
		write_error_and_exit("Coordinates are invalid; save_camera");
	if (save_vector(&(data->camera.vector), split[1]) == -1)
		write_error_and_exit("Vector not normalized; save_camera");
	if (check_fov(&(data->camera.fov), split[2]) == -1)
		write_error_and_exit("Fov is invalid; save_camera");
	free_array(split);
}

void	save_light(t_data *data, char *line)
{
	char	**split;

	line++;
	data->light_is_set += 1;
	if (data->light_is_set > 1)
		write_error_and_exit("More than one light; save_light");
	if (is_line_invalid(line, " -,."))
		write_error_and_exit("Forbidden chars; save_light");
	split = ft_split(line, ' ');
	if (!split)
		write_error_and_exit("Malloc failed; save_light");
	if (count_split(split) != 3)
		write_error_and_exit("Wrong word count; save_light");
	if (save_coordinates(&(data->light.place), split[0]) == -1)
		write_error_and_exit("Coordinates are invalid; save_light");
	if (save_ratio(&(data->light.ratio), split[1]) == -1)
		write_error_and_exit("Wrong ratio; save_light");
	if (save_rgb(&(data->light.rgb), split[2]) == -1)
		write_error_and_exit("Wrong rgb; save_light");
	free_array(split);
}
