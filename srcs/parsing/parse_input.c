/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse_input.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/12 17:59:47 by akant         #+#    #+#                 */
/*   Updated: 2022/03/21 11:11:11 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	parse_and_save_line(t_data *data, char *str)
{
	if (!ft_strncmp(str, "\n", 1))
		return ;
	if (!ft_strncmp(str, "A ", 2))
		save_ambient(data, str);
	else if (!ft_strncmp(str, "C ", 2))
		save_camera(data, str);
	else if (!ft_strncmp(str, "L ", 2))
		save_light(data, str);
	else if (!ft_strncmp(str, "pl ", 3))
		save_plane(data, str);
	else if (!ft_strncmp(str, "sp ", 3))
		save_sphere(data, str);
	else if (!ft_strncmp(str, "cy ", 3))
		save_cylinder(data, str);
	else if (ft_strncmp(str, "\0", 1) == 0)
		return ;
	else
		write_error_and_exit("Wrong element in .rt file");
}

void	check_file_extension(char *file)
{
	char	*extension;

	extension = ft_strchr(file, '.');
	if (!extension)
		write_error_and_exit("Invalid file name");
	if (ft_strcmp(extension, ".rt") != 0)
		write_error_and_exit("Please provide a file with .rt extension");
}

void	parse_input(t_data *data, char *file)
{
	int		ret;
	int		fd;
	char	*line;

	ret = 1;
	check_file_extension(file);
	fd = open(file, O_RDONLY);
	if (fd < 0)
		perror_and_exit(errno);
	while (ret > 0)
	{
		ret = get_next_line(fd, &line);
		if (ret < 0)
			write_error_and_exit("Read or malloc failed; gnl");
		parse_and_save_line(data, line);
		free(line);
	}
	if (!is_acl_complete(*data))
		write_error_and_exit("Scene element missing; parse input");
	close(fd);
}
