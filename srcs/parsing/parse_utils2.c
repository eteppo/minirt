/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   parse_utils2.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/13 14:40:15 by akant         #+#    #+#                 */
/*   Updated: 2022/02/23 15:41:21 by akant         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

int	count_split(char **split)
{
	int	count;

	count = 0;
	while (split && split[count])
		count++;
	return (count);
}

int	is_in_range(float number, float lower, float upper)
{
	if (number < lower || number > upper)
		return (0);
	return (1);
}

int	is_in_set(char c, char *set)
{
	int	i;

	i = 0;
	if (set)
	{
		while (set[i])
		{
			if (set[i] == c)
				return (1);
			i++;
		}
	}
	return (0);
}

int	is_line_invalid(char *str, char *allowed_chars)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (!ft_isdigit(str[i]) && !is_in_set(str[i], allowed_chars))
			return (1);
		i++;
	}
	return (0);
}

int	check_fov(int *fov, char *line)
{
	int	error;

	error = 0;
	*fov = ft_atoi_with_error(line, &error);
	if (*fov < 0 || *fov > 180)
		return (-1);
	return (error);
}
