/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   list.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/14 12:41:05 by akant         #+#    #+#                 */
/*   Updated: 2022/03/09 12:11:45 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

int	ft_obj_lstsize(t_obj_list *lst)
{
	int	len;

	len = 0;
	while (lst)
	{
		lst = lst->next;
		len++;
	}
	return (len);
}

t_obj_list	*ft_obj_lstnew(t_object *object, char object_type)
{
	t_obj_list	*new_elem;

	new_elem = malloc(sizeof(t_obj_list));
	if (!new_elem)
		return (0);
	new_elem->object_type = object_type;
	new_elem->object = *object;
	new_elem->next = NULL;
	return (new_elem);
}

void	ft_obj_lstadd_back(t_obj_list **lst, t_obj_list *new)
{
	t_obj_list	*last;

	last = *lst;
	new->next = NULL;
	if (*lst == NULL)
	{
		*lst = new;
		return ;
	}
	while (last->next != NULL)
	{
		last = last->next;
	}
	last->next = new;
}

int	save_to_object_list(t_data *data, t_object *object, char obj_type)
{
	t_obj_list	*new_object;

	new_object = ft_obj_lstnew(object, obj_type);
	if (!new_object)
		return (-1);
	ft_obj_lstadd_back(&(data->object_list), new_object);
	return (0);
}
