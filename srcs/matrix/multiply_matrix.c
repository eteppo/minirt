/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   multiply_matrix.c                                  :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/17 14:41:19 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/09 13:33:02 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

t_float3	float3_matrix_multiplied(float *matrix, t_float3 old_vector,
		int fourth_value)
{
	t_float3	new_vector;

	new_vector.x = old_vector.x * matrix[0] + old_vector.y * matrix[4]
		+ old_vector.z * matrix[8] + fourth_value * matrix[12];
	new_vector.y = old_vector.x * matrix[1] + old_vector.y * matrix[5]
		+ old_vector.z * matrix[9] + fourth_value * matrix[13];
	new_vector.z = old_vector.x * matrix[2] + old_vector.y * matrix[6]
		+ old_vector.z * matrix[10] + fourth_value * matrix[14];
	return (new_vector);
}

void	matrix_multiplication(float *matrix1, float *matrix2, float *result)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (i < 4)
	{
		while (j < 4)
		{
			result[4 * i + j] = matrix1[i * 4] * matrix2[j]
				+ matrix1[i * 4 + 1] * matrix2[4 + j]
				+ matrix1[i * 4 + 2] * matrix2[8 + j]
				+ matrix1[i * 4 + 3] * matrix2[12 + j];
			j++;
		}
		j = 0;
		i++;
	}
}
