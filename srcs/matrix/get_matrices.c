/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   get_matrices.c                                     :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/23 14:35:14 by akant         #+#    #+#                 */
/*   Updated: 2022/03/18 17:04:38 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	get_cylinder_matrices(t_cylinder cylinder, t_matrices *matrix)
{
	t_ray		cyl_orientation;

	cyl_orientation.place = cylinder.center;
	cyl_orientation.vector = cylinder.vector;
	create_matrix(matrix->final, cyl_orientation, 1);
	create_invert_matrix(matrix->final, matrix->inverse);
}

void	get_camera_matrix(t_scene *scene)
{
	t_ray		camera;

	camera.place = scene->data.camera.place;
	camera.vector = scene->data.camera.vector;
	create_matrix(scene->data.cam_to_world_matrix, camera, 0);
}
