/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   matrix.c                                           :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/17 14:41:19 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/18 17:03:41 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

t_float3	compute_forward(float *matrix, t_ray ray)
{
	matrix[8] = ray.vector.x;
	matrix[9] = ray.vector.y;
	matrix[10] = ray.vector.z;
	matrix[11] = 0;
	return (ray.vector);
}

float	absolute_value(float value)
{
	if (value < 0)
		value *= -1;
	return (value);
}

t_float3	compute_right(float *matrix, t_float3 forward, int is_cylinder)
{
	t_float3	right;
	t_float3	temp;

	temp.x = 0;
	temp.y = 1;
	temp.z = 0;
	if (absolute_value(forward.y) == 1)
	{
		temp.z = -forward.y;
		temp.y = 0;
	}
	else if (is_cylinder && absolute_value(forward.y) >= 0.707)
	{
		temp.x = 1;
		if (forward.y < 0)
			temp.x = -1;
		temp.y = 0;
	}
	right = cross_product(temp, forward);
	matrix[0] = right.x;
	matrix[1] = right.y;
	matrix[2] = right.z;
	matrix[3] = 0;
	return (right);
}

void	compute_up(float *matrix, t_float3 forward, t_float3 right)
{
	t_float3	up;

	up = cross_product(forward, right);
	matrix[4] = up.x;
	matrix[5] = up.y;
	matrix[6] = up.z;
	matrix[7] = 0;
}

void	create_matrix(float *matrix, t_ray ray, int is_cylinder)
{
	t_float3	forward;
	t_float3	right;

	forward = compute_forward(matrix, ray);
	right = compute_right(matrix, forward, is_cylinder);
	compute_up(matrix, forward, right);
	matrix[12] = ray.place.x;
	matrix[13] = ray.place.y;
	matrix[14] = ray.place.z;
	matrix[15] = 1;
}
