/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   init.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/12 17:45:55 by eteppo        #+#    #+#                 */
/*   Updated: 2022/02/23 12:17:10 by akant         ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	init_float3(t_float3 *location)
{
	location->x = 0;
	location->y = 0;
	location->z = 0;
}

t_int3	init_rgb(void)
{
	t_int3	rgb;

	rgb.x = 0;
	rgb.y = 0;
	rgb.z = 0;
	return (rgb);
}

void	init_screen(t_screen *screen, int width, int height)
{
	screen->mlx = mlx_init();
	if (!screen->mlx)
		write_error_and_exit("Failed to set up the connection.");
	screen->window = mlx_new_window(screen->mlx, width, height, "MiniRT");
	if (!screen->window)
		write_error_and_exit("Failed to create a new window.");
	screen->image_ptr = mlx_new_image(screen->mlx, width, height);
	if (!screen->image_ptr)
		write_error_and_exit("Failed to create a new image.");
	screen->address = mlx_get_data_addr(screen->image_ptr,
			&screen->bits_per_pixel, &screen->line_length, &screen->endian);
}

void	init_data(t_data *data, int width, int height)
{
	data->width = width;
	data->height = height;
	data->object_list = NULL;
	data->ambient_is_set = 0;
	data->camera_is_set = 0;
	data->light_is_set = 0;
}

void	init_scene(t_scene *scene, int width, int height)
{
	t_screen	screen;
	t_data		data;

	init_screen(&screen, width, height);
	init_data(&data, width, height);
	scene->screen = screen;
	scene->data = data;
}
