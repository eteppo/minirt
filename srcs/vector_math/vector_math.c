/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   vector_math.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/23 12:12:00 by akant         #+#    #+#                 */
/*   Updated: 2022/03/22 17:56:50 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

float	dot_product(t_float3 a, t_float3 b)
{
	return (a.x * b.x + a.y * b.y + a.z * b.z);
}

t_float3	cross_product(t_float3 vector_a, t_float3 vector_b)
{
	t_float3	new;

	new.x = vector_a.y * vector_b.z - vector_a.z * vector_b.y;
	new.y = vector_a.z * vector_b.x - vector_a.x * vector_b.z;
	new.z = vector_a.x * vector_b.y - vector_a.y * vector_b.x;
	return (new);
}

float	calculate_vector_length(t_float3 vector)
{
	return (sqrt(pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2)));
}

void	normalize_vector(t_float3 *vector)
{
	float	len;
	float	inverted_len;

	len = calculate_vector_length(*vector);
	if (len > 0)
	{
		inverted_len = 1.0 / len;
		vector->x *= inverted_len;
		vector->y *= inverted_len;
		vector->z *= inverted_len;
	}
}

t_float3	get_cylinder_normal(t_hitpoint *hitpoint, t_ray ray)
{
	t_float3	normal;

	if (is_in_cylinder(ray.place, *hitpoint))
		hitpoint->inside_cylinder = 1;
	if (hitpoint->cylinder_hitpart == 'u' || hitpoint->cylinder_hitpart == 'l')
	{
		normal = hitpoint->node->object.cylinder.vector;
		if ((hitpoint->cylinder_hitpart == 'l' && !hitpoint->inside_cylinder)
			|| (hitpoint->cylinder_hitpart == 'u' && hitpoint->inside_cylinder))
			normal = float3_value_multiplication(normal, -1);
	}
	else
	{
		normal = float3_subtraction(hitpoint->place,
				hitpoint->hitpoint_projection);
		if (hitpoint->inside_cylinder)
			normal = float3_value_multiplication(normal, -1);
	}
	return (normal);
}
