/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   float3_math.c                                      :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2021/12/01 12:49:23 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/22 12:29:19 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

t_float3	float3_addition(t_float3 a, t_float3 b)
{
	t_float3	sum;

	sum.x = a.x + b.x;
	sum.y = a.y + b.y;
	sum.z = a.z + b.z;
	return (sum);
}

t_float3	float3_subtraction(t_float3 value, t_float3 subtractor)
{
	t_float3	difference;

	difference.x = value.x - subtractor.x;
	difference.y = value.y - subtractor.y;
	difference.z = value.z - subtractor.z;
	return (difference);
}

t_float3	float3_value_multiplication(t_float3 vec, float value)
{
	t_float3	temp;

	temp.x = vec.x * value;
	temp.y = vec.y * value;
	temp.z = vec.z * value;
	return (temp);
}

void	calculate_intersection_coordinates(t_hitpoint *hitpoint, t_ray ray)
{
	if (hitpoint->distance_to_camera == INFINITY)
		return ;
	hitpoint->place = float3_addition(ray.place,
			float3_value_multiplication(ray.vector,
				hitpoint->distance_to_camera));
}
