/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   hitpoint_cylinder.c                                :+:    :+:            */
/*                                                     +:+                    */
/*   By: eteppo <eteppo@student.codam.nl>             +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/03/07 09:28:27 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/09 14:21:45 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	find_local_intersection_cylinder(t_hitpoint *hitpoint_middle,
		t_cylinder cylinder, t_ray ray)
{
	t_abc_form	abc;

	abc.a = ray.vector.x * ray.vector.x + ray.vector.y * ray.vector.y;
	abc.b = 2 * (ray.place.x * ray.vector.x + ray.place.y * ray.vector.y);
	abc.c = (ray.place.x * ray.place.x) + (ray.place.y * ray.place.y)
		- ((cylinder.diameter * 0.5) * (cylinder.diameter * 0.5));
	if (!solve_quadratic(&abc))
		return ;
	if (abc.root1 < 0)
	{
		abc.root1 = abc.root2;
		if (abc.root1 < 0)
			return ;
	}
	hitpoint_middle->distance_to_camera = abc.root1;
	calculate_intersection_coordinates(hitpoint_middle, ray);
	hitpoint_middle->hitpoint_projection.x = 0;
	hitpoint_middle->hitpoint_projection.y = 0;
	hitpoint_middle->hitpoint_projection.z = hitpoint_middle->place.z;
	if (!(hitpoint_middle->place.z >= cylinder.height * -0.5
			&& hitpoint_middle->place.z <= cylinder.height * 0.5))
		hitpoint_middle->distance_to_camera = INFINITY;
}

t_hitpoint	get_hitpoint_middle(t_ray local_ray, t_hitpoint hitpoint,
		t_matrices matrix, char hitpart)
{
	t_hitpoint	hitpoint_middle;

	hitpoint_middle.node = hitpoint.node;
	hitpoint_middle.cylinder_hitpart = hitpart;
	hitpoint_middle.distance_to_camera = INFINITY;
	find_local_intersection_cylinder(&hitpoint_middle,
		hitpoint.node->object.cylinder, local_ray);
	if (hitpoint_middle.distance_to_camera < hitpoint.distance_to_camera)
		return (hitpoint_middle);
	return (hitpoint);
}

void	find_local_intersection_disc(t_hitpoint *hitpoint,
		t_ray local_disc, t_ray local_ray, float diameter)
{
	float		denom;
	t_float3	ray_to_disc_center;
	t_float3	surface_vector;
	float		t;
	float		d2;

	init_float3(&ray_to_disc_center);
	hitpoint->distance_to_camera = INFINITY;
	denom = dot_product(local_disc.vector, local_ray.vector);
	if (denom > 1e-6 || denom < -1e-6)
	{
		ray_to_disc_center = float3_subtraction(local_disc.place,
				local_ray.place);
		t = dot_product(ray_to_disc_center, local_disc.vector) / denom;
		if (t > 0)
		{
			hitpoint->distance_to_camera = t;
			calculate_intersection_coordinates(hitpoint, local_ray);
			surface_vector = float3_subtraction(hitpoint->place,
					local_disc.place);
			d2 = dot_product(surface_vector, surface_vector);
			if (d2 > diameter * 0.5 * diameter * 0.5)
				hitpoint->distance_to_camera = INFINITY;
		}
	}
}

void	save_local_disc(t_ray *local_disc, t_hitpoint hitpoint, char hitpart)
{
	init_float3(&local_disc->place);
	init_float3(&local_disc->vector);
	local_disc->place.z = hitpoint.node->object.cylinder.height * 0.5;
	local_disc->vector.z = 1;
	if (hitpart == 'l')
	{
		local_disc->place.z *= -1;
		local_disc->vector.z *= -1;
	}
}

t_hitpoint	get_hitpoint_disc(t_ray local_ray, t_hitpoint hitpoint,
		t_matrices matrix, char hitpart)
{
	t_ray		local_disc;
	t_hitpoint	disc_hitpoint;

	disc_hitpoint.cylinder_hitpart = hitpart;
	disc_hitpoint.distance_to_camera = INFINITY;
	disc_hitpoint.node = hitpoint.node;
	save_local_disc(&local_disc, hitpoint, hitpart);
	find_local_intersection_disc(&disc_hitpoint, local_disc, local_ray,
		hitpoint.node->object.cylinder.diameter);
	if (disc_hitpoint.distance_to_camera < hitpoint.distance_to_camera)
		return (disc_hitpoint);
	return (hitpoint);
}
