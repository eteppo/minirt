/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   utils.c                                            :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/02/23 12:20:27 by akant         #+#    #+#                 */
/*   Updated: 2022/03/09 14:53:11 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	my_mlx_pixel_put(t_scene scene, int x, int y, unsigned int color)
{
	char	*dst;
	int		offset;

	offset = y * scene.screen.line_length
		+ x * (scene.screen.bits_per_pixel / 8);
	dst = scene.screen.address + offset;
	*(unsigned int *)dst = color;
}

void	quadratic_extended(t_abc_form *abc, float q, float discriminant)
{
	float	temp;

	q = -0.5 * (abc->b - sqrt(discriminant));
	if (abc->b > 0)
		q = -0.5 * (abc->b + sqrt(discriminant));
	abc->root1 = q / abc->a;
	abc->root2 = abc->c / q;
	if (abc->root1 > abc->root2)
	{
		temp = abc->root1;
		abc->root1 = abc->root2;
		abc->root2 = temp;
	}
}

int	solve_quadratic(t_abc_form *abc)
{
	float	discriminant;
	float	q;

	q = 0;
	discriminant = abc->b * abc->b - 4 * abc->a * abc->c;
	if (discriminant < 0)
		return (0);
	else if (discriminant == 0)
	{
		abc->root1 = -0.5 * abc->b / abc->a;
		abc->root2 = abc->root1;
	}
	else
		quadratic_extended(abc, q, discriminant);
	return (1);
}

int	is_in_sphere(t_scene scene, t_hitpoint hitpoint)
{
	float	x;
	float	y;
	float	z;

	x = scene.data.camera.place.x - hitpoint.node->object.sphere.center.x;
	y = scene.data.camera.place.y - hitpoint.node->object.sphere.center.y;
	z = scene.data.camera.place.z - hitpoint.node->object.sphere.center.z;
	return (pow(x, 2) + pow(y, 2) + pow(z, 2)
		< pow(hitpoint.node->object.sphere.diameter * 0.5, 2));
}

int	is_in_cylinder(t_float3 ray_place, t_hitpoint hitpoint)
{
	float		x;
	float		y;
	t_matrices	matrix;
	t_ray		local_ray;

	get_cylinder_matrices(hitpoint.node->object.cylinder, &matrix);
	local_ray.place = float3_matrix_multiplied(matrix.inverse, ray_place, 1);
	x = local_ray.place.x - hitpoint.node->object.cylinder.center.x;
	y = local_ray.place.y - hitpoint.node->object.cylinder.center.y;
	if (pow(x, 2) + pow(y, 2)
		< pow(hitpoint.node->object.cylinder.diameter * 0.5, 2)
		&& local_ray.place.z > hitpoint.node->object.cylinder.height * -0.5
		&& local_ray.place.z < hitpoint.node->object.cylinder.height * 0.5)
		return (1);
	return (0);
}
