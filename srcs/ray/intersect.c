/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   intersect.c                                        :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/17 14:41:19 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/22 17:56:55 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

t_hitpoint	intersect_sphere(t_ray ray, t_obj_list *node)
{
	t_float3	l_vector;
	t_abc_form	abc;
	t_hitpoint	hitpoint;

	l_vector = float3_subtraction(ray.place, node->object.sphere.center);
	abc.a = dot_product(ray.vector, ray.vector);
	abc.b = 2 * dot_product(ray.vector, l_vector);
	abc.c = dot_product(l_vector, l_vector)
		- ((node->object.sphere.diameter * 0.5)
			* (node->object.sphere.diameter * 0.5));
	hitpoint.node = node;
	if (!solve_quadratic(&abc))
	{
		hitpoint.distance_to_camera = INFINITY;
		return (hitpoint);
	}
	if (abc.root1 < 0)
	{
		abc.root1 = abc.root2;
		if (abc.root1 < 0)
			abc.root1 = INFINITY;
	}
	hitpoint.distance_to_camera = abc.root1;
	calculate_intersection_coordinates(&hitpoint, ray);
	return (hitpoint);
}

t_hitpoint	intersect_plane(t_ray ray, t_obj_list *node)
{
	float		denom;
	t_float3	new_vector;
	t_hitpoint	hitpoint;
	float		ray_len;

	init_float3(&new_vector);
	denom = dot_product(node->object.plane.vector, ray.vector);
	hitpoint.distance_to_camera = INFINITY;
	if (denom > 1e-6 || denom < -1e-6)
	{
		new_vector = float3_subtraction(node->object.plane.center, ray.place);
		ray_len = dot_product(new_vector, node->object.plane.vector) / denom;
		if (ray_len > 0)
		{
			hitpoint.distance_to_camera = ray_len;
			hitpoint.node = node;
			calculate_intersection_coordinates(&hitpoint, ray);
		}
	}
	return (hitpoint);
}

t_hitpoint	intersect_cylinder(t_ray ray, t_obj_list *node)
{
	t_hitpoint	hitpoint;
	t_matrices	matrix;
	t_ray		local_ray;

	hitpoint.node = node;
	hitpoint.distance_to_camera = INFINITY;
	get_cylinder_matrices(node->object.cylinder, &matrix);
	local_ray.place = float3_matrix_multiplied(matrix.inverse, ray.place, 1);
	local_ray.vector = float3_matrix_multiplied(matrix.inverse, ray.vector, 0);
	normalize_vector(&(local_ray.vector));
	hitpoint = get_hitpoint_disc(local_ray, hitpoint, matrix, 'u');
	hitpoint = get_hitpoint_middle(local_ray, hitpoint, matrix, 'm');
	hitpoint = get_hitpoint_disc(local_ray, hitpoint, matrix, 'l');
	if (hitpoint.distance_to_camera < INFINITY)
	{
		hitpoint.place = float3_matrix_multiplied(matrix.final,
				hitpoint.place, 1);
		if (hitpoint.cylinder_hitpart == 'm')
			hitpoint.hitpoint_projection = float3_matrix_multiplied(
					matrix.final, hitpoint.hitpoint_projection, 1);
	}
	return (hitpoint);
}

t_hitpoint	intersect_object_list(t_ray ray, t_obj_list *node)
{
	t_hitpoint	closest_hitpoint;
	t_hitpoint	hitpoint;

	closest_hitpoint.distance_to_camera = INFINITY;
	while (node)
	{
		if (node->object_type == 's')
			hitpoint = intersect_sphere(ray, node);
		else if (node->object_type == 'p')
			hitpoint = intersect_plane(ray, node);
		else if (node->object_type == 'c')
			hitpoint = intersect_cylinder(ray, node);
		if (hitpoint.distance_to_camera < closest_hitpoint.distance_to_camera
			&& hitpoint.distance_to_camera > 0)
			closest_hitpoint = hitpoint;
		node = node->next;
	}
	return (closest_hitpoint);
}

t_hitpoint	light_intersect_object_list(t_ray ray, t_obj_list *node,
	float max_distance)
{
	t_hitpoint	closest_hitpoint;
	t_hitpoint	hitpoint;

	closest_hitpoint.distance_to_camera = INFINITY;
	while (node)
	{
		if (node->object_type == 's')
			hitpoint = intersect_sphere(ray, node);
		else if (node->object_type == 'p')
			hitpoint = intersect_plane(ray, node);
		else if (node->object_type == 'c')
			hitpoint = intersect_cylinder(ray, node);
		if (hitpoint.distance_to_camera < closest_hitpoint.distance_to_camera
			&& hitpoint.distance_to_camera > 0 && hitpoint.distance_to_camera
			< max_distance)
			closest_hitpoint = hitpoint;
		node = node->next;
	}
	return (closest_hitpoint);
}
