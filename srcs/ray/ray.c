/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   ray.c                                              :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/17 14:41:19 by eteppo        #+#    #+#                 */
/*   Updated: 2022/03/22 17:55:51 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

void	create_lightray(t_scene scene, t_hitpoint hitpoint, t_ray *lightray)
{
	lightray->place = scene.data.light.place;
	lightray->vector = float3_subtraction(hitpoint.place,
			scene.data.light.place);
	normalize_vector(&(lightray->vector));
}

int	is_object_lit(t_scene scene, t_hitpoint *hitpoint, t_ray ray,
	t_ray lightray)
{
	float		facing_ratio;
	float		ray_angle;

	if (hitpoint->node->object_type == 'p')
		hitpoint->normal = hitpoint->node->object.plane.vector;
	else if (hitpoint->node->object_type == 's')
	{
		hitpoint->normal = float3_subtraction(hitpoint->place,
				hitpoint->node->object.sphere.center);
		if (is_in_sphere(scene, *hitpoint))
			hitpoint->normal = float3_value_multiplication
				(hitpoint->normal, -1);
	}
	else
		hitpoint->normal = get_cylinder_normal(hitpoint, ray);
	normalize_vector(&(hitpoint->normal));
	facing_ratio = dot_product(hitpoint->normal,
			float3_value_multiplication(lightray.vector, -1));
	ray_angle = dot_product(hitpoint->normal,
			float3_value_multiplication(ray.vector, -1));
	if ((facing_ratio < 0 && ray_angle < 0) || (facing_ratio > 0
			&& ray_angle > 0) || (facing_ratio == 0 && ray_angle == 0))
		return (1);
	return (0);
}

t_ray	create_ray(t_scene scene, int x, int y)
{
	t_ray	ray;

	ray.place = scene.data.camera.place;
	ray.vector.x = (2 * ((x + 0.5) / scene.data.width) - 1)
		* ((float)scene.data.width / scene.data.height)
		* (tan((scene.data.camera.fov / 2) * (M_PI / 180)));
	ray.vector.y = (1 - 2 * ((y + 0.5) / scene.data.height))
		* (tan((scene.data.camera.fov / 2) * (M_PI / 180)));
	ray.vector.z = 1;
	ray.vector = float3_matrix_multiplied(scene.data.cam_to_world_matrix,
			ray.vector, 0);
	normalize_vector(&(ray.vector));
	return (ray);
}

void	shoot_ray(t_scene scene, int x, int y)
{
	t_ray			ray;
	unsigned int	rgb;
	t_hitpoint		hitpoint;
	t_ray			lightray;

	ray = create_ray(scene, x, y);
	create_lightray(scene, hitpoint, &lightray);
	hitpoint = intersect_object_list(ray, scene.data.object_list);
	rgb = 0;
	if (hitpoint.distance_to_camera < INFINITY)
	{
		hitpoint.is_object_lit = 0;
		hitpoint.inside_cylinder = 0;
		if (is_object_lit(scene, &hitpoint, ray, lightray))
			hitpoint.is_object_lit = 1;
		rgb = calculate_color(&scene, hitpoint);
	}
	my_mlx_pixel_put(scene, x, y, rgb);
}

void	build_camera_rays(t_scene scene)
{
	int	x;
	int	y;

	get_camera_matrix(&scene);
	y = 0;
	while (y < scene.data.height)
	{
		x = 0;
		while (x < scene.data.width)
		{
			shoot_ray(scene, x, y);
			x++;
		}
		y++;
	}
}
