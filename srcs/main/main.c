/* ************************************************************************** */
/*                                                                            */
/*                                                        ::::::::            */
/*   main.c                                             :+:    :+:            */
/*                                                     +:+                    */
/*   By: akant <akant@student.codam.nl>               +#+                     */
/*                                                   +#+                      */
/*   Created: 2022/01/12 16:32:29 by akant         #+#    #+#                 */
/*   Updated: 2022/03/22 13:40:49 by eteppo        ########   odam.nl         */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minirt.h"

int	key_hook(int keycode, t_scene *scene)
{
	if (scene && keycode == 53)
		exit(0);
	return (0);
}

int	close_window(t_scene *scene)
{
	if (scene)
		exit(0);
	return (0);
}

int	main(int argc, char **argv)
{
	t_scene	scene;

	if (argc != 2)
		perror_and_exit(EINVAL);
	init_scene(&scene, 1280, 720);
	parse_input(&(scene.data), argv[1]);
	build_camera_rays(scene);
	mlx_put_image_to_window(scene.screen.mlx, scene.screen.window,
		scene.screen.image_ptr, 0, 0);
	mlx_key_hook(scene.screen.window, key_hook, &scene);
	mlx_hook(scene.screen.window, 17, 0, close_window, &scene);
	mlx_loop(scene.screen.mlx);
	return (0);
}
